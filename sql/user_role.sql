DROP TABLE IF EXISTS `blog`.`posts`;
CREATE TABLE posts (
  id INT(11) NOT NULL AUTO_INCREMENT UNIQUE,
  title TEXT NOT NULL,
  text TEXT NOT NULL,
  PRIMARY KEY (id));

DROP TABLE IF EXISTS `blog`.`users`;
CREATE  TABLE users (
  id INT(11) NOT NULL AUTO_INCREMENT UNIQUE,
  username VARCHAR(45) NOT NULL UNIQUE,
  password VARCHAR(60) NOT NULL,
  email VARCHAR(255) NOT NULL,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (id));

DROP TABLE IF EXISTS `blog`.`roles`;
CREATE TABLE roles (
  id INT(11) NOT NULL AUTO_INCREMENT UNIQUE,
  user_id INT NOT NULL,
  ROLE VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY uni_user_role (ROLE,user_id),
  KEY fk_user_idx (user_id),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id));

INSERT INTO users(username,password,email,enabled)
VALUES ('admin','$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 'admin@admin.ru', TRUE); /*password: 123456*/
INSERT INTO users(username,password,email,enabled)
VALUES ('user','$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 'user@user.ru', TRUE);  /*password: 123456*/

INSERT INTO roles (user_id, ROLE)
VALUES ('1', 'ROLE_USER');
INSERT INTO roles (user_id, ROLE)
VALUES ('1', 'ROLE_ADMIN');
INSERT INTO roles (user_id, ROLE)
VALUES ('2', 'ROLE_USER');

INSERT INTO posts (title, text)
VALUES ('Lorem Ipsum', 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.');
INSERT INTO posts (title, text)
VALUES ('Откуда он появился?', 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла"), написанной Цицероном в 45 году н.э. Этот трактат по теории этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, "Lorem ipsum dolor sit amet..", происходит от одной из строк в разделе 1.10.32 Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33 "de Finibus Bonorum et Malorum" Цицерона и их английский перевод, сделанный H. Rackham, 1914 год.');
INSERT INTO posts (title, text)
VALUES ('Почему он используется?', 'Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).');