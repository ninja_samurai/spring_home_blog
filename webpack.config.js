var path = require('path');

module.exports = {
  context: path.join(__dirname, 'src/main/js'), // исходная директория
  entry: './app', // файл для сборки, если несколько - указываем hash (entry name => filename)
  output: {
    path: path.join(__dirname, 'src/main/webapp/resources/js'), // выходная директория
    filename: "build.js"
  },
  module: {
    loaders: [
      {test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }}
    ]
  }
};