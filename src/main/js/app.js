import ReactDOM from "react-dom";
import Routes from "./router/Router.react";

ReactDOM.render(Routes, document.getElementById('content'));