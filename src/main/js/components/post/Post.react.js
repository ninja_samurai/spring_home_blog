import React from "react";
import PostStore from "../../stores/PostStore";
import isMounted from "../../utils/MountUtil";

function getPostState(id) {
    return {
        post: PostStore.getPost(id)
    };
}
export class Post extends React.Component {

    constructor(props) {
        super(props);
        let id = this.props.params.id;
        this.state = getPostState(id);
    }

    componentDidMount() {
        PostStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        PostStore.removeChangeListener(this._onChange.bind(this));
    }

    _onChange() {
        if (isMounted(this)) {
            let id = this.props.params.id;
            this.setState(getPostState(id))
        }
    }

    render() {
        let post = this.state.post;
        if (typeof post !== 'undefined' && post !== null) {
            return (<div className="main">
                <h2>{post.title}</h2>
                <p>{post.text}</p>
            </div>);
        }
        return (<div><h3>Load or not found</h3></div>);
    }
}