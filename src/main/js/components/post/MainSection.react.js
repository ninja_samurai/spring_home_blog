import React from "react";
import PostActions from "../../actions/PostActions";
import PostStore from "../../stores/PostStore";
import {PostList} from "./PostList.react.js";
import isMounted from "../../utils/MountUtil";

function getMainState() {
    return {
        posts: PostStore.all()
    };
}

export class MainSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = getMainState();
    }

    componentDidMount() {
        PostStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        PostStore.removeChangeListener(this._onChange.bind(this));
    }

    _onChange() {
        if (isMounted(this)) {
            this.setState(getMainState())
        }
    }

    render() {
        var pages = [];
        var postList = this.state.posts;
        for (var key in postList) {
            var post = postList[key];
            pages.push(<PostList key={key} post={post} id={key}/>);
        }
        return (<div className="main">
            <div className="ui items" id="todo-list">{pages}</div>
        </div>);
    }
}
