import React from "react";
var ReactPropTypes = React.PropTypes;
import {Link} from 'react-router';

import classNames from "classnames";

export class PostList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var post = this.props.post;
        var identification = this.props.id;
        var name = "item post-" + post.id;
        var url = "/post/" + identification;
        return (
            <div
                className={name}
                key={post.id}>
                <div className="image">
                    <img src="/resources/img/img_none.gif" />
                </div>
                <div className="content">
                    <a className="header">Заголовок</a>
                    <div className="meta">
                        <span>Description</span>
                    </div>
                    <div className="description">
                        <p>{post.text}</p>
                    </div>
                    <div className="extra"><Link className="item" to={url}>Подробнее</Link></div>
                </div>
            </div>
        );
    }
}

PostList.propTypes = {
    id: ReactPropTypes.string.isRequired,
    post: ReactPropTypes.object.isRequired };