import React from "react";
var ReactPropTypes = React.PropTypes;
import UserStore from "../../stores/UserStore";
import isMounted from "../../utils/MountUtil";
import {Link} from 'react-router';

function getHeaderState() {
    return {
        user: UserStore.getUser()
    };
}

export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = getHeaderState();
    }
    componentDidMount() {
        UserStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange.bind(this));
    }

    _onChange() {
        if (isMounted(this)) {
            this.setState(getHeaderState())
        }
    }

    render() {
        let auth = this.state.user.auth;
        let authMenu = (auth)? <Link className="item" to="/logout">Logout</Link>: <Link className="item" to="/login">Sign-in</Link>;
        return (
            <header id="header">
                <div className="ui top fixed menu">
                    <div className="item"><img src="/resources/img/logo.png" /></div>
                    <Link className="item" to="/posts">Posts</Link>
                    {authMenu}
                    <Link className="item" to="/registration">Registration</Link>
                </div>
            </header>
        );
    }

}