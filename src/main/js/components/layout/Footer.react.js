import React from "react";
var ReactPropTypes = React.PropTypes;

export class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ui inverted vertical footer segment form-page">
                <div className="ui container">
                    Vladimir Danilov 2016. All Rights Reserved
                </div>
            </div>
        );
    }

}