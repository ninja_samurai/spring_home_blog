import {Footer} from "./layout/Footer.react.js";
import {Header} from "./layout/Header.react.js";
import PostActions from "../actions/PostActions";

import React from "react";


export class MainApp extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        var headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Login-Ajax-call": 'true'
        };
        $.ajax({
            url: "/rest/posts",
            dataType: 'json',
            headers: headers,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: (data) => {
                PostActions.setAllPosts(data);
            },
            error: (request, status, error) => {
                console.log("error:", error);
            }
        });

    }

    render() {
        return (
            <div className="content">
                <Header />
                {this.props.children}
                <Footer />
            </div>
        );
    }
}