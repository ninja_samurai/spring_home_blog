import React from "react";
import UserActions from "../../actions/UserActions";

export class Login extends React.Component {
    constructor(props) {
        super(props);
    }

    sendFormLogin(e) {
        e.preventDefault();

        let data = $("#loginForm").serialize();
        var csrf = $("meta[name='_csrf']").attr("content");
        var csrfHeader = $("meta[name='_csrf_header']").attr("content");
        var headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Login-Ajax-call": 'true'
        };
        headers[csrfHeader] = csrf;
        $.ajax({
            type: "POST",
            url: '/authenticate',
            data: data,
            headers: headers
        }).then((data) => {
            if(data === "ok") {
                let user = {
                    auth: true,
                    username: ""
                };
                UserActions.login(user);
                this.props.history.pushState(null, '/');
            }
        }).fail((request, status, error) => {
            console.log("error:", error);
        });
    }

    render() {
        return (
            <form id="loginForm" className="ui large form container" method="post" action="/authenticate" onSubmit={this.sendFormLogin.bind(this)}>
                <div className="field">
                    <label>Login</label>
                    <input type="text" name="username" placeholder="Login"/>
                </div>
                <div className="field">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password"/>
                </div>
                <button className="ui blue button" type="submit">Submit</button>
            </form>
        );
    }
}