import React from "react";

export class Registration extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.history.pushState(null, '/');
    }

    sendFormLogin(e) {
        e.preventDefault();
        console.log("click")
    }

    render() {
        return (
            <form className="ui large form container" method="post" action="/rest/login" onSubmit={this.sendFormLogin}>
                <div className="field">
                    <label>Login</label>
                    <input type="text" name="login" placeholder="Login"/>
                </div>
                <div className="field">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password"/>
                </div>
                <div className="field">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm-password" placeholder="Password"/>
                </div>
                <div className="field">
                    <div className="ui checkbox">
                        <input type="checkbox" tabIndex="0" className="hidden"/>
                        <label>I agree to the Terms and Conditions</label>
                    </div>
                </div>
                <button className="ui teal button" type="submit">Submit</button>
            </form>
        );
    }
}