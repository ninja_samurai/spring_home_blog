import ReactDOM from "react-dom";

const isMounted = (component) => {
    try {
        ReactDOM.findDOMNode(component);
        return true;
    } catch (e) {
        return false;
    }
};
export default isMounted;