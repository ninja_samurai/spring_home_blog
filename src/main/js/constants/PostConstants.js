import keyMirror from "keymirror";

module.exports = keyMirror({
    GET_POSTS: null,
    SET_POSTS: null
});