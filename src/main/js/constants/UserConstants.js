import keyMirror from "keymirror";

module.exports = keyMirror({
    LOGIN: null,
    LOGOUT: null
});