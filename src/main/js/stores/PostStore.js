import AppDispatcher from "../dispatcher/AppDispatcher";
import EventEmitter from "events";
import PostConstants from "../constants/PostConstants";
import assign from "object-assign";

var CHANGE_EVENT = 'change';
var EventsEm = EventEmitter.EventEmitter;
var _posts = [];

class PostStore extends EventEmitter {

    all() {
        return _posts;
    }

    getPost(id) {
        return _posts[id];
    }

    fillPosts(posts) {
        _posts = posts;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

}
let postStore = new PostStore();

postStore.dispatcherIndex = AppDispatcher.register((payload) => {
    var action = payload.actionType;
    switch (action) {
        case PostConstants.SET_POSTS:
            let posts = payload.posts;
            postStore.fillPosts(posts);
            break;
    }
    postStore.emitChange();
    return true;
});

export default postStore;