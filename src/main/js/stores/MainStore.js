import AppDispatcher from "../dispatcher/AppDispatcher";
import EventEmitter from "events";
import MainConstants from "../constants/MainConstants";
import assign from "object-assign";

var CHANGE_EVENT = 'change';
var EventsEm = EventEmitter.EventEmitter;
var _todos = {};

function create(text) {
    var id = Date.now();
    _todos[id] = {
        id: id,
        complete: false,
        text: text
    };
}

function destroy(id) {
    delete _todos[id];
}

var MainStore = assign({}, EventsEm.prototype, {
    areAllComplete: function() {
        for (var id in _todos) {
            if (!_todos[id].complete) {
                return false;
            }
        }
        return true;
    },
    getAll: function() {
        return _todos;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    dispatcherIndex: AppDispatcher.register(function(payload) {
        var action = payload.actionType;
        var text;

        switch(action) {
            case MainConstants.TODO_CREATE:
                text = payload.text.trim();
                if (text !== '') {
                    create(text);
                    MainStore.emitChange();
                }
                break;

            case MainConstants.TODO_DESTROY:
                destroy(payload.id);
                MainStore.emitChange();
                break;

        }

        return true;
    })

});

module.exports = MainStore;