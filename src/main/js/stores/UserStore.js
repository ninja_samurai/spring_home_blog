import AppDispatcher from "../dispatcher/AppDispatcher";
import EventEmitter from "events";
import UserConstants from "../constants/UserConstants";
import assign from "object-assign";

var CHANGE_EVENT = 'change';
var EventsEm = EventEmitter.EventEmitter;
var _user = {
    auth: false,
    username: ""
};

class UserStore extends EventEmitter {

    getUser() {
        return _user;
    }

    login(user) {
        _user.auth = true;
        _user.username = user.username;
    }

    logout() {
        _user.auth = false;
        _user.username = "";
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

}
let userStore = new UserStore();

userStore.dispatcherIndex = AppDispatcher.register((payload) => {
    var action = payload.actionType;
    switch (action) {
        case UserConstants.LOGIN:
            let user = payload.user;
            userStore.login(user);
            break;
        case UserConstants.LOGOUT:
            userStore.logout();
    }
    userStore.emitChange();
    return true;
});

export default userStore;