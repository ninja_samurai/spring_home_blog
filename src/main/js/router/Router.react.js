import React from "react";

import { MainApp } from "../components/MainApp.react";
import { MainSection } from "../components/post/MainSection.react";
import { Post } from "../components/post/Post.react";
import { Login } from "../components/auth/Login.react";
import { Registration } from "../components/auth/Registration.react";
import { Router, Route, Link, IndexRoute } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';

let history = createBrowserHistory();

const Routes = (<Router history={history}>
    <Route path="/" component={MainApp}>
        <IndexRoute component={MainSection} />
        <Route path="/posts" component={MainSection} />
        <Route path="/post/:id" component={Post} />
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Login} />
        <Route path="/registration" component={Registration} />
    </Route>
</Router>);

export default Routes;