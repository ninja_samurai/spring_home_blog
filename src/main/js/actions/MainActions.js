import AppDispatcher from "../dispatcher/AppDispatcher";
import MainConstants from "../constants/MainConstants";

var MainActions = {

    create: function(text) {
        AppDispatcher.dispatch({
            actionType: MainConstants.TODO_CREATE,
            text: text
        });
    },

    updateText: function(id, text) {
        AppDispatcher.dispatch({
            actionType: MainConstants.TODO_UPDATE_TEXT,
            id: id,
            text: text
        });
    },

    toggleComplete: function(todo) {
        var id = todo.id;
        var actionType = todo.complete ?
            MainConstants.TODO_UNDO_COMPLETE :
            MainConstants.TODO_COMPLETE;

        AppDispatcher.dispatch({
            actionType: actionType,
            id: id
        });
    },

    toggleCompleteAll: function() {
        AppDispatcher.dispatch({
            actionType: MainConstants.TODO_TOGGLE_COMPLETE_ALL
        });
    },

    destroy: function(id) {
        AppDispatcher.dispatch({
            actionType: MainConstants.TODO_DESTROY,
            id: id
        });
    },

    destroyCompleted: function() {
        AppDispatcher.dispatch({
            actionType: MainConstants.TODO_DESTROY_COMPLETED
        });
    }

};

module.exports = MainActions;