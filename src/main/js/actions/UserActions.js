import AppDispatcher from "../dispatcher/AppDispatcher";
import UserConstants from "../constants/UserConstants";

let UserActions = {
    logout: () => {
        AppDispatcher.dispatch({
            actionType: UserConstants.LOGOUT
        });
    },
    login: (user) => {
        AppDispatcher.dispatch({
            actionType: UserConstants.LOGIN,
            user: user
        });
    }
};
export default UserActions;