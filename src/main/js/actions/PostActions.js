import AppDispatcher from "../dispatcher/AppDispatcher";
import PostConstants from "../constants/PostConstants";

let PostActions = {
    getAllPosts: () => {
        AppDispatcher.dispatch({
            actionType: PostConstants.GET_POSTS
        });
    },
    setAllPosts: (posts) => {
        AppDispatcher.dispatch({
            actionType: PostConstants.SET_POSTS,
            posts: posts
        });
    }
};
export default PostActions;