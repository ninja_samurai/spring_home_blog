package com.blog.web.validation;

import com.blog.web.dto.UserDto;
import com.blog.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidation implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDto.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto user = (UserDto) o;
        if(!errors.hasFieldErrors("username")) {
            UserDto userExist = userService.getUserByName(user.getUsername());
            if(userExist != null) {
                errors.rejectValue("user", "Username exist");
            }
        }
        if(user.getPassword().isEmpty()) {
            errors.rejectValue("password", "Password not be empty");
        }
        if(user.getConfirmPassword().isEmpty()){
            errors.rejectValue("confirmPassword", "Confirm Password not be empty");
        }
        if(!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("password", "Confirm Password not equals Password");
        }
        if(user.getEmail().isEmpty()) {
            errors.rejectValue("email", "Email not be empty");
        }
    }
}
