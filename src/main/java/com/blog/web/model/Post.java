package com.blog.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="posts")
public class Post extends AbstractEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        System.out.println("POST TITLE");
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
