package com.blog.web.dao;

import com.blog.web.model.User;

/**
 * Created by dz on 03.11.15.
 */
public interface UserDao {

    User findUserByName(String name);

    void saveUser(User user);
}
