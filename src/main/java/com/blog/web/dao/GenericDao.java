package com.blog.web.dao;

import com.blog.web.model.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by dz on 17.10.15.
 */
public interface GenericDao<T extends AbstractEntity> {

    Class<T> tType();

    SessionFactory getSessionFactory();

    default List<T> getAll() {
        Session session = getSessionFactory().openSession();
        List<T> result = session.createCriteria(tType()).list();
        session.close();
        return result;
    }

    default T load(Long id) {
        Session session = getSessionFactory().openSession();
        T t = session.load(tType(), id);
        session.close();
        return t;
    }
    default void save(T entity) {
        Session session = getSessionFactory().openSession();
        Transaction tx =  session.beginTransaction();
        session.saveOrUpdate(entity);
        tx.commit();
        session.close();
    }

    default String getName() {
        System.out.println("getName");
        return "Get Name";
    }

}
