package com.blog.web.dao.impl;

import com.blog.web.dao.PostDao;
import com.blog.web.model.Post;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dz on 17.10.15.
 */
@Repository
public class PostDaoImpl implements PostDao {

    @Autowired
    protected SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }


    @Cacheable(value="movieFindCache")
    public List<Post> getAll(){
        Session session = getSessionFactory().openSession();
        List<Post> result = session.createCriteria(Post.class).list();
        session.close();
        return result;
    }

    public Post load(Long id) {
        Session session = getSessionFactory().openSession();
        Post t = session.load(Post.class, id);
        session.close();
        return t;
    }
}
