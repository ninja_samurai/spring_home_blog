package com.blog.web.dao.impl;

import com.blog.web.dao.GenericDao;
import com.blog.web.dao.UserDao;
import com.blog.web.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by dz on 03.11.15.
 */
@Repository
public class UserDaoImpl implements UserDao, GenericDao<User> {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public User findUserByName(String username) {
        Session session = getSessionFactory().openSession();

        User user = (User) session.createCriteria(tType())
                .add(Restrictions.eq("username", username))
                .uniqueResult();

        return user;
    }

    @Override
    public Class<User> tType() {
        return User.class;
    }

    @Autowired
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public void saveUser(User user) {
        save(user);
    }
}
