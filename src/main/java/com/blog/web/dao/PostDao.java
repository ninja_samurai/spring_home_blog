package com.blog.web.dao;

import com.blog.web.model.Post;

import java.util.List;

public interface PostDao {
    List<Post> getAll();
    Post load(Long id);
}
