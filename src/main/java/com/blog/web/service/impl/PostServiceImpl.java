package com.blog.web.service.impl;

import com.blog.web.dao.PostDao;
import com.blog.web.dto.PostDto;
import com.blog.web.model.Post;
import com.blog.web.service.ModelMapperService;
import com.blog.web.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl extends ModelMapperService<PostDto, Post> implements PostService {

    @Autowired
    PostDao postDao;

    @Override
    public List<PostDto> getAll() {
        List<Post> posts = postDao.getAll();
        List<PostDto> postDtos = this.convertModelToDto(posts, PostDto.class);
        return postDtos;
    }

    @Override
    public PostDto getPost(Long id) {
        Post post = postDao.load(id);
        PostDto postDto = this.convertToDto(post, PostDto.class);
        return postDto;
    }


}
