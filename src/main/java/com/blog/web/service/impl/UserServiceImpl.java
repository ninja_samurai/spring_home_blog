package com.blog.web.service.impl;

import com.blog.web.dao.UserDao;
import com.blog.web.dto.UserDto;
import com.blog.web.model.User;
import com.blog.web.service.ModelMapperService;
import com.blog.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ModelMapperService<UserDto, User> implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDto getUserByName(String username) {
        User user =  userDao.findUserByName(username);
        UserDto postDto = this.convertToDto(user, UserDto.class);
        return postDto;
    }
    @Override
    public User getUserByNameUser(String username) {
        User user =  userDao.findUserByName(username);
        return user;
    }

    @Override
    public void saveUser(UserDto userDto) {
        User user = this.convertToModel(userDto, User.class);
        userDao.saveUser(user);
    }
}
