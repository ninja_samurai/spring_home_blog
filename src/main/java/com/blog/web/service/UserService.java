package com.blog.web.service;

import com.blog.web.dto.UserDto;
import com.blog.web.model.User;

public interface UserService {

    UserDto getUserByName(String username);
    User getUserByNameUser(String username);

    void saveUser(UserDto userDto);
}
