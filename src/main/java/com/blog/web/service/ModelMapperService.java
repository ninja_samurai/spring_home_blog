package com.blog.web.service;

import com.blog.web.dto.AbstractDto;
import com.blog.web.model.AbstractEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class ModelMapperService<A extends AbstractDto, B extends AbstractEntity> {

    @Autowired
    protected ModelMapper modelMapper;

    public List<B> convertDtoToModel(final List<A> input, Class<B> bClass) {
        return input.stream().map((a) -> this.convertToModel(a, bClass)).collect(Collectors.toList());
    }
    public List<A> convertModelToDto(final List<B> input, Class<A> aClass) {
        return input.stream().map((b) -> this.convertToDto(b, aClass)).collect(Collectors.toList());
    }
    public A convertToDto(B b, Class<A>aClass){
        return modelMapper.map(b, aClass);
    }
    public B convertToModel(A a, Class<B> bClass){
        return modelMapper.map(a, bClass);
    }
}
