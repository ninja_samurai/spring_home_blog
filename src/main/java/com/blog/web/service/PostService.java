package com.blog.web.service;

import com.blog.web.dto.PostDto;

import java.util.List;

public interface PostService {

    List<PostDto>getAll();

    PostDto getPost(Long id);
}
