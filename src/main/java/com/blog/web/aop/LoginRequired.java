package com.blog.web.aop;

import java.lang.annotation.*;

/**
 * Created by dz on 18.11.15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface LoginRequired {
}
