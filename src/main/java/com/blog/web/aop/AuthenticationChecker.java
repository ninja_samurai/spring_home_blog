package com.blog.web.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by dz on 18.11.15.
 */
@Aspect
@Component
public class AuthenticationChecker {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationChecker.class);

    @Pointcut("@annotation(com.blog.web.aop.LoginRequired)")
    public void loginR(){}

    @Before("loginR()")
    public void authChecked() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(auth instanceof AnonymousAuthenticationToken){
            LOGGER.info("anon");
        } else {
            LOGGER.info("user");
        }
    }
}
