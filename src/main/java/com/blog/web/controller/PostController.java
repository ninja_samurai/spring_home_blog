package com.blog.web.controller;

import com.blog.web.dto.PostDto;
import com.blog.web.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/rest")
public class PostController {

    @Autowired
    private PostService postService;

    @RequestMapping("/posts")
    public @ResponseBody
    List<PostDto> getAllPost(){
        List<PostDto> postsList = postService.getAll();
        return postsList;
    }
    @RequestMapping("/post/{id}")
    public @ResponseBody PostDto getPost(@PathVariable Long id){
        PostDto postDto = postService.getPost(id);
        return postDto;
    }
    @RequestMapping(value = "/post/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    PostDto createPost(@RequestBody PostDto postDto){
        return postDto;
    }

}
