package com.blog.web.controller;

import com.blog.web.aop.LoginRequired;
import com.blog.web.dto.PostDto;
import com.blog.web.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Controller
@RequestMapping("/")
public class DefaultController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultController.class);

    @Autowired
    private PostService postService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(){
        System.out.println("index");
        return "index";
    }

    @LoginRequired
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public @ResponseBody List<PostDto> welcomePage() {

        List<PostDto> posts = postService.getAll();
        LOGGER.info("INFO");
        return posts;
    }
}
