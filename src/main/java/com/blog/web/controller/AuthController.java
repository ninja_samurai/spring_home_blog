package com.blog.web.controller;

import com.blog.web.dto.UserDto;
import com.blog.web.model.User;
import com.blog.web.service.UserService;
import com.blog.web.validation.UserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private UserValidation userValidation;

    @Autowired
    private UserService userService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public @ResponseBody
    User success(Principal principal){
        User user = new User();
        user.setUsername(principal.getName());
        return user;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public @ResponseBody
    UserDto createUser(@Valid UserDto user, BindingResult bindingResult){
        userValidation.validate(user, bindingResult);
        if(bindingResult.hasErrors()){
            user.setErrorList(bindingResult.getAllErrors());
        } else {
            userService.saveUser(user);
        }
        return user;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout, HttpServletRequest request, ModelMap model) {

        if (error != null) {
            model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
        }
        if (logout != null) {
            model.addAttribute("msg", "You've been logged out successfully.");
        }
        return "login";
    }

    private String getErrorMessage(HttpServletRequest request, String key) {

        Exception exception = (Exception) request.getSession().getAttribute(key);

        String error;
        if (exception instanceof BadCredentialsException) {
            error = "Invalid username and password!";
        } else if (exception instanceof LockedException) {
            error = exception.getMessage();
        } else {
            error = "Invalid username and password!";
        }

        return error;
    }

}
