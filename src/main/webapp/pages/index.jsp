<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spring Blog</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.1.4.min.js"></script>
    <script src="/resources/js/semantic.min.js"></script>
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
<div id="content"></div>
<script src="/resources/js/build.js"></script>
</body>
</html>
